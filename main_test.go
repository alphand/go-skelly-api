package main

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMain(t *testing.T) {
	Convey("Given No Parameter", t, func() {
		So(isDevelopment, ShouldEqual, true)
		So(port, ShouldEqual, 3000)
	})
}
