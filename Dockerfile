FROM alpine:latest

MAINTAINER Niko Darmawan <niko@nikodarmawan.com>

WORKDIR "/opt"

ADD .docker_build/mailee /opt/bin/mailee

CMD ["/opt/bin/mailee"]

