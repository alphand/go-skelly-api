# GO_BUILD_ENV := GOOS=linux GOARCH=amd64
DOCKER_BUILD = $(SHELL pwd).docker_build
APP_CMD := mailee
DOCKER_CMD = $(DOCKER_BUILD)/$(APP_CMD)

$(DOCKER_CMD):	clean
	mkdir -p $(DOCKER_BUILD)
	$(GO_BUILD_ENV) go build -v -o $(DOCKER_CMD) ./main.go

webserver:
	$(GO_BUILD_ENV) go build -v -o $(APP_CMD) ./main.go

clean:
	rm -rf $(DOCKER_BUILD)

test:
	go test $(go list ./... | grep -v /vendor/)

heroku: $(DOCKER_CMD)
	heroku container:push web
