package utils_test

import (
	"net/http"
	"testing"

	"net/http/httptest"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/alphand/mailee/utils"
)

func TestNewJSONResult(t *testing.T) {
	Convey("Given Response with JSONResult", t, func() {
		rr := new(httptest.ResponseRecorder)

		Convey("When valid code:200 response", func() {
			res := utils.NewJSONResult(http.StatusOK, "success")
			res.Render(rr, http.StatusOK)

			So(rr.Code, ShouldEqual, 200)
			So(rr.Header().Get("Content-Type"), ShouldEqual, "application/json")
		})
	})
}
