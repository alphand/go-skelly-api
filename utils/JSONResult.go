package utils

import (
	"encoding/json"
	"net/http"
	"time"
)

type JSONResult struct {
	Code      int
	Response  interface{}
	Timestamp int64
}

func (js *JSONResult) Render(w http.ResponseWriter, StatusCode int) {
	res, _ := json.Marshal(js)

	w.WriteHeader(StatusCode)
	w.Header().Set("Content-Type", "application/json")

	w.Write(res)
}

func NewJSONResult(status int, response interface{}) *JSONResult {
	return &JSONResult{
		status,
		response,
		time.Now().UnixNano(),
	}
}
