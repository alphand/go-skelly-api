package controllers

import (
	"net/http"

	"gitlab.com/alphand/mailee/utils"

	"github.com/gorilla/mux"
)

type MainCtrlImpl struct{}

func NewMainController() *MainCtrlImpl {
	return &MainCtrlImpl{}
}

func (mc *MainCtrlImpl) Register(router *mux.Router) {
	router.HandleFunc("/", mc.Home)
	router.HandleFunc("/{rest:.*}", mc.Error404)
}

func (mc *MainCtrlImpl) Home(w http.ResponseWriter, req *http.Request) {
	res := utils.NewJSONResult(http.StatusOK, "Welcome to mailee API Gateway")
	res.Render(w, http.StatusOK)
}

func (mc *MainCtrlImpl) Error404(w http.ResponseWriter, req *http.Request) {
	res := utils.NewJSONResult(http.StatusNotFound, "Resource not found")
	res.Render(w, http.StatusNotFound)
}
