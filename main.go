package main

import (
	"flag"

	"strconv"

	"gitlab.com/alphand/mailee/web"
)

var port int
var isDevelopment bool

func init() {
	flag.IntVar(&port, "port", 3000, "setting port for webserver")
	flag.BoolVar(&isDevelopment, "is-dev", true, "is this dev env?")

	flag.Parse()
}

func main() {
	s := web.NewServer(isDevelopment)
	s.Run(":" + strconv.Itoa(port))
}
