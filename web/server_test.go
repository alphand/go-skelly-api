package web_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/alphand/mailee/web"
)

func TestServer(t *testing.T) {
	Convey("Given new server can be set up", t, func() {
		Convey("Server is setup with development mode", func() {
			s := web.NewServer(true)
			So(s, ShouldNotBeNil)
		})
	})
}
