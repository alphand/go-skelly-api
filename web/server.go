package web

import (
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"gitlab.com/alphand/mailee/controllers"
)

type Server struct {
	*negroni.Negroni
}

func NewServer(
	isDevelopment bool,
) *Server {

	s := Server{
		negroni.Classic(),
	}

	router := mux.NewRouter()

	mainctrl := controllers.NewMainController()
	mainctrl.Register(router)

	s.UseHandler(router)
	return &s
}
